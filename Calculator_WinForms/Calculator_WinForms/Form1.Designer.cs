﻿namespace Calculator_WinForms
{
    partial class CalcForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.resultTB = new System.Windows.Forms.TextBox();
            this.backButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.sqrButton = new System.Windows.Forms.Button();
            this.signButton = new System.Windows.Forms.Button();
            this.sqrtButton = new System.Windows.Forms.Button();
            this.reverseButton = new System.Windows.Forms.Button();
            this.divideButton = new System.Windows.Forms.Button();
            this.nineButton = new System.Windows.Forms.Button();
            this.multipleButton = new System.Windows.Forms.Button();
            this.MinusButton = new System.Windows.Forms.Button();
            this.modButton = new System.Windows.Forms.Button();
            this.eightButton = new System.Windows.Forms.Button();
            this.sevenButton = new System.Windows.Forms.Button();
            this.sixButton = new System.Windows.Forms.Button();
            this.fiveButton = new System.Windows.Forms.Button();
            this.threeButton = new System.Windows.Forms.Button();
            this.twoButton = new System.Windows.Forms.Button();
            this.oneButton = new System.Windows.Forms.Button();
            this.fourButton = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.dotButton = new System.Windows.Forms.Button();
            this.zeroButtton = new System.Windows.Forms.Button();
            this.resultButton = new System.Windows.Forms.Button();
            this.resultLabel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // resultTB
            // 
            this.resultTB.BackColor = System.Drawing.Color.Azure;
            this.resultTB.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultTB.ForeColor = System.Drawing.Color.RoyalBlue;
            this.resultTB.Location = new System.Drawing.Point(12, 32);
            this.resultTB.Name = "resultTB";
            this.resultTB.ReadOnly = true;
            this.resultTB.Size = new System.Drawing.Size(246, 26);
            this.resultTB.TabIndex = 0;
            this.resultTB.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.resultTB.WordWrap = false;
            // 
            // backButton
            // 
            this.backButton.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.backButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.backButton.ForeColor = System.Drawing.Color.DodgerBlue;
            this.backButton.Location = new System.Drawing.Point(6, 62);
            this.backButton.Name = "backButton";
            this.backButton.Size = new System.Drawing.Size(46, 45);
            this.backButton.TabIndex = 1;
            this.backButton.Text = "<<";
            this.backButton.UseVisualStyleBackColor = false;
            this.backButton.Click += new System.EventHandler(this.backButton_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.cancelButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cancelButton.ForeColor = System.Drawing.Color.DodgerBlue;
            this.cancelButton.Location = new System.Drawing.Point(58, 62);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(45, 45);
            this.cancelButton.TabIndex = 2;
            this.cancelButton.Text = "C";
            this.cancelButton.UseVisualStyleBackColor = false;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // sqrButton
            // 
            this.sqrButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.sqrButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sqrButton.ForeColor = System.Drawing.Color.LightCyan;
            this.sqrButton.Location = new System.Drawing.Point(109, 62);
            this.sqrButton.Name = "sqrButton";
            this.sqrButton.Size = new System.Drawing.Size(47, 42);
            this.sqrButton.TabIndex = 3;
            this.sqrButton.Text = "x^2";
            this.sqrButton.UseVisualStyleBackColor = false;
            this.sqrButton.Click += new System.EventHandler(this.sqrButton_Click);
            // 
            // signButton
            // 
            this.signButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.signButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.signButton.ForeColor = System.Drawing.Color.LightCyan;
            this.signButton.Location = new System.Drawing.Point(159, 62);
            this.signButton.Name = "signButton";
            this.signButton.Size = new System.Drawing.Size(46, 42);
            this.signButton.TabIndex = 4;
            this.signButton.Text = "+/-";
            this.signButton.UseVisualStyleBackColor = false;
            this.signButton.Click += new System.EventHandler(this.signButton_Click);
            // 
            // sqrtButton
            // 
            this.sqrtButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.sqrtButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sqrtButton.ForeColor = System.Drawing.Color.LightCyan;
            this.sqrtButton.Location = new System.Drawing.Point(212, 62);
            this.sqrtButton.Name = "sqrtButton";
            this.sqrtButton.Size = new System.Drawing.Size(46, 42);
            this.sqrtButton.TabIndex = 5;
            this.sqrtButton.Text = "√";
            this.sqrtButton.UseVisualStyleBackColor = false;
            this.sqrtButton.Click += new System.EventHandler(this.operatorButton_Click);
            // 
            // reverseButton
            // 
            this.reverseButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.reverseButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reverseButton.ForeColor = System.Drawing.Color.LightCyan;
            this.reverseButton.Location = new System.Drawing.Point(212, 113);
            this.reverseButton.Name = "reverseButton";
            this.reverseButton.Size = new System.Drawing.Size(46, 42);
            this.reverseButton.TabIndex = 10;
            this.reverseButton.Text = "1/x";
            this.reverseButton.UseVisualStyleBackColor = false;
            this.reverseButton.Click += new System.EventHandler(this.reverseButton_Click);
            // 
            // divideButton
            // 
            this.divideButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.divideButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.divideButton.ForeColor = System.Drawing.Color.LightCyan;
            this.divideButton.Location = new System.Drawing.Point(159, 113);
            this.divideButton.Name = "divideButton";
            this.divideButton.Size = new System.Drawing.Size(46, 42);
            this.divideButton.TabIndex = 9;
            this.divideButton.Text = "/";
            this.divideButton.UseVisualStyleBackColor = false;
            this.divideButton.Click += new System.EventHandler(this.operatorButton_Click);
            // 
            // nineButton
            // 
            this.nineButton.BackColor = System.Drawing.Color.Navy;
            this.nineButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nineButton.ForeColor = System.Drawing.Color.LightCyan;
            this.nineButton.Location = new System.Drawing.Point(109, 113);
            this.nineButton.Name = "nineButton";
            this.nineButton.Size = new System.Drawing.Size(47, 42);
            this.nineButton.TabIndex = 8;
            this.nineButton.Text = "9";
            this.nineButton.UseVisualStyleBackColor = false;
            this.nineButton.Click += new System.EventHandler(this.button_Click);
            // 
            // multipleButton
            // 
            this.multipleButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.multipleButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.multipleButton.ForeColor = System.Drawing.Color.LightCyan;
            this.multipleButton.Location = new System.Drawing.Point(159, 164);
            this.multipleButton.Name = "multipleButton";
            this.multipleButton.Size = new System.Drawing.Size(45, 41);
            this.multipleButton.TabIndex = 7;
            this.multipleButton.Text = "*";
            this.multipleButton.UseVisualStyleBackColor = false;
            this.multipleButton.Click += new System.EventHandler(this.operatorButton_Click);
            // 
            // MinusButton
            // 
            this.MinusButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.MinusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.MinusButton.ForeColor = System.Drawing.Color.LightCyan;
            this.MinusButton.Location = new System.Drawing.Point(160, 211);
            this.MinusButton.Name = "MinusButton";
            this.MinusButton.Size = new System.Drawing.Size(46, 42);
            this.MinusButton.TabIndex = 6;
            this.MinusButton.Text = "-";
            this.MinusButton.UseVisualStyleBackColor = false;
            this.MinusButton.Click += new System.EventHandler(this.operatorButton_Click);
            // 
            // modButton
            // 
            this.modButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.modButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.modButton.ForeColor = System.Drawing.Color.LightCyan;
            this.modButton.Location = new System.Drawing.Point(212, 160);
            this.modButton.Name = "modButton";
            this.modButton.Size = new System.Drawing.Size(46, 46);
            this.modButton.TabIndex = 15;
            this.modButton.Text = "%";
            this.modButton.UseVisualStyleBackColor = false;
            this.modButton.Click += new System.EventHandler(this.operatorButton_Click);
            // 
            // eightButton
            // 
            this.eightButton.BackColor = System.Drawing.Color.Navy;
            this.eightButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.eightButton.ForeColor = System.Drawing.Color.LightCyan;
            this.eightButton.Location = new System.Drawing.Point(57, 113);
            this.eightButton.Name = "eightButton";
            this.eightButton.Size = new System.Drawing.Size(46, 42);
            this.eightButton.TabIndex = 14;
            this.eightButton.Text = "8";
            this.eightButton.UseVisualStyleBackColor = false;
            this.eightButton.Click += new System.EventHandler(this.button_Click);
            // 
            // sevenButton
            // 
            this.sevenButton.BackColor = System.Drawing.Color.Navy;
            this.sevenButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sevenButton.ForeColor = System.Drawing.Color.LightCyan;
            this.sevenButton.Location = new System.Drawing.Point(7, 113);
            this.sevenButton.Name = "sevenButton";
            this.sevenButton.Size = new System.Drawing.Size(44, 42);
            this.sevenButton.TabIndex = 13;
            this.sevenButton.Text = "7";
            this.sevenButton.UseVisualStyleBackColor = false;
            this.sevenButton.Click += new System.EventHandler(this.button_Click);
            // 
            // sixButton
            // 
            this.sixButton.BackColor = System.Drawing.Color.Navy;
            this.sixButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.sixButton.ForeColor = System.Drawing.Color.LightCyan;
            this.sixButton.Location = new System.Drawing.Point(109, 164);
            this.sixButton.Name = "sixButton";
            this.sixButton.Size = new System.Drawing.Size(45, 42);
            this.sixButton.TabIndex = 12;
            this.sixButton.Text = "6";
            this.sixButton.UseVisualStyleBackColor = false;
            this.sixButton.Click += new System.EventHandler(this.button_Click);
            // 
            // fiveButton
            // 
            this.fiveButton.BackColor = System.Drawing.Color.Navy;
            this.fiveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fiveButton.ForeColor = System.Drawing.Color.LightCyan;
            this.fiveButton.Location = new System.Drawing.Point(57, 164);
            this.fiveButton.Name = "fiveButton";
            this.fiveButton.Size = new System.Drawing.Size(46, 42);
            this.fiveButton.TabIndex = 11;
            this.fiveButton.Text = "5";
            this.fiveButton.UseVisualStyleBackColor = false;
            this.fiveButton.Click += new System.EventHandler(this.button_Click);
            // 
            // threeButton
            // 
            this.threeButton.BackColor = System.Drawing.Color.Navy;
            this.threeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.threeButton.ForeColor = System.Drawing.Color.LightCyan;
            this.threeButton.Location = new System.Drawing.Point(109, 212);
            this.threeButton.Name = "threeButton";
            this.threeButton.Size = new System.Drawing.Size(47, 42);
            this.threeButton.TabIndex = 18;
            this.threeButton.Text = "3";
            this.threeButton.UseVisualStyleBackColor = false;
            this.threeButton.Click += new System.EventHandler(this.button_Click);
            // 
            // twoButton
            // 
            this.twoButton.BackColor = System.Drawing.Color.Navy;
            this.twoButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.twoButton.ForeColor = System.Drawing.Color.LightCyan;
            this.twoButton.Location = new System.Drawing.Point(58, 211);
            this.twoButton.Name = "twoButton";
            this.twoButton.Size = new System.Drawing.Size(45, 42);
            this.twoButton.TabIndex = 17;
            this.twoButton.Text = "2";
            this.twoButton.UseVisualStyleBackColor = false;
            this.twoButton.Click += new System.EventHandler(this.button_Click);
            // 
            // oneButton
            // 
            this.oneButton.BackColor = System.Drawing.Color.Navy;
            this.oneButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.oneButton.ForeColor = System.Drawing.Color.LightCyan;
            this.oneButton.Location = new System.Drawing.Point(6, 211);
            this.oneButton.Name = "oneButton";
            this.oneButton.Size = new System.Drawing.Size(46, 42);
            this.oneButton.TabIndex = 16;
            this.oneButton.Text = "1";
            this.oneButton.UseVisualStyleBackColor = false;
            this.oneButton.Click += new System.EventHandler(this.button_Click);
            // 
            // fourButton
            // 
            this.fourButton.BackColor = System.Drawing.Color.Navy;
            this.fourButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.fourButton.ForeColor = System.Drawing.Color.LightCyan;
            this.fourButton.Location = new System.Drawing.Point(7, 164);
            this.fourButton.Name = "fourButton";
            this.fourButton.Size = new System.Drawing.Size(44, 42);
            this.fourButton.TabIndex = 19;
            this.fourButton.Text = "4";
            this.fourButton.UseVisualStyleBackColor = false;
            this.fourButton.Click += new System.EventHandler(this.button_Click);
            // 
            // plusButton
            // 
            this.plusButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.plusButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.plusButton.ForeColor = System.Drawing.Color.LightCyan;
            this.plusButton.Location = new System.Drawing.Point(161, 260);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(44, 42);
            this.plusButton.TabIndex = 21;
            this.plusButton.Text = "+";
            this.plusButton.UseVisualStyleBackColor = false;
            this.plusButton.Click += new System.EventHandler(this.operatorButton_Click);
            // 
            // dotButton
            // 
            this.dotButton.BackColor = System.Drawing.Color.CornflowerBlue;
            this.dotButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.dotButton.ForeColor = System.Drawing.Color.LightCyan;
            this.dotButton.Location = new System.Drawing.Point(109, 260);
            this.dotButton.Name = "dotButton";
            this.dotButton.Size = new System.Drawing.Size(47, 42);
            this.dotButton.TabIndex = 20;
            this.dotButton.Text = ",";
            this.dotButton.UseVisualStyleBackColor = false;
            this.dotButton.Click += new System.EventHandler(this.button_Click);
            // 
            // zeroButtton
            // 
            this.zeroButtton.BackColor = System.Drawing.Color.Navy;
            this.zeroButtton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.zeroButtton.ForeColor = System.Drawing.Color.LightCyan;
            this.zeroButtton.Location = new System.Drawing.Point(6, 259);
            this.zeroButtton.Name = "zeroButtton";
            this.zeroButtton.Size = new System.Drawing.Size(97, 42);
            this.zeroButtton.TabIndex = 22;
            this.zeroButtton.Text = "0";
            this.zeroButtton.UseVisualStyleBackColor = false;
            this.zeroButtton.Click += new System.EventHandler(this.button_Click);
            // 
            // resultButton
            // 
            this.resultButton.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.resultButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultButton.ForeColor = System.Drawing.Color.Crimson;
            this.resultButton.Location = new System.Drawing.Point(212, 211);
            this.resultButton.Name = "resultButton";
            this.resultButton.Size = new System.Drawing.Size(46, 91);
            this.resultButton.TabIndex = 23;
            this.resultButton.Text = "=";
            this.resultButton.UseVisualStyleBackColor = false;
            this.resultButton.Click += new System.EventHandler(this.resultButton_Click);
            // 
            // resultLabel
            // 
            this.resultLabel.AutoSize = true;
            this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resultLabel.Location = new System.Drawing.Point(12, 7);
            this.resultLabel.Name = "resultLabel";
            this.resultLabel.Size = new System.Drawing.Size(0, 18);
            this.resultLabel.TabIndex = 24;
            // 
            // CalcForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(270, 313);
            this.Controls.Add(this.resultLabel);
            this.Controls.Add(this.resultButton);
            this.Controls.Add(this.zeroButtton);
            this.Controls.Add(this.plusButton);
            this.Controls.Add(this.dotButton);
            this.Controls.Add(this.fourButton);
            this.Controls.Add(this.threeButton);
            this.Controls.Add(this.twoButton);
            this.Controls.Add(this.oneButton);
            this.Controls.Add(this.modButton);
            this.Controls.Add(this.eightButton);
            this.Controls.Add(this.sevenButton);
            this.Controls.Add(this.sixButton);
            this.Controls.Add(this.fiveButton);
            this.Controls.Add(this.reverseButton);
            this.Controls.Add(this.divideButton);
            this.Controls.Add(this.nineButton);
            this.Controls.Add(this.multipleButton);
            this.Controls.Add(this.MinusButton);
            this.Controls.Add(this.sqrtButton);
            this.Controls.Add(this.signButton);
            this.Controls.Add(this.sqrButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.backButton);
            this.Controls.Add(this.resultTB);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "CalcForm";
            this.Text = "Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox resultTB;
        private System.Windows.Forms.Button backButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button sqrButton;
        private System.Windows.Forms.Button signButton;
        private System.Windows.Forms.Button sqrtButton;
        private System.Windows.Forms.Button reverseButton;
        private System.Windows.Forms.Button divideButton;
        private System.Windows.Forms.Button nineButton;
        private System.Windows.Forms.Button multipleButton;
        private System.Windows.Forms.Button MinusButton;
        private System.Windows.Forms.Button modButton;
        private System.Windows.Forms.Button eightButton;
        private System.Windows.Forms.Button sevenButton;
        private System.Windows.Forms.Button sixButton;
        private System.Windows.Forms.Button fiveButton;
        private System.Windows.Forms.Button threeButton;
        private System.Windows.Forms.Button twoButton;
        private System.Windows.Forms.Button oneButton;
        private System.Windows.Forms.Button fourButton;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button dotButton;
        private System.Windows.Forms.Button zeroButtton;
        private System.Windows.Forms.Button resultButton;
        private System.Windows.Forms.Label resultLabel;
    }
}

