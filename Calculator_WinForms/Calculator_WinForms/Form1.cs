﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator_WinForms
{
    public partial class CalcForm : Form
    {
        double resultValue = 0;
        private double resultValue2 = 0;
        string operationPerformed = null;
        bool operrationIsPressed = false;

        public CalcForm()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, EventArgs e)
        {
            if (resultValue.ToString() == resultTB.Text)
            {
                resultValue2 =double.Parse(resultTB.Text);
            }
            if ((resultTB.Text == "0")||(operrationIsPressed))
            {
                resultTB.Clear();
            }
            
            
                operrationIsPressed = false;
                Button button = (Button) sender;
                if (button.Text == ",")
                {
                    if (!resultTB.Text.Contains(","))
                    {
                        resultTB.Text = resultTB.Text + button.Text;
                    }
                }
                else 
                    resultTB.Text = resultTB.Text + button.Text;
            

            //resultLabel.Text = resultTB.Text + resultValue;
        }

        private void operatorButton_Click(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            if (resultValue == 0)
            {
                operationPerformed = button.Text; 
                resultButton.PerformClick();
                
                resultLabel.Text = resultValue + " " + operationPerformed;
                operrationIsPressed = true;
            }
            else
            {
                if (button.Text == "√")
                {
                    operationPerformed = "√";
                    resultValue = Double.Parse(resultTB.Text);
                    resultLabel.Text = operationPerformed + resultValue;
                    operrationIsPressed = true;
                }
                else
                {
                    if (resultValue2 == resultValue)
                    {
                        operationPerformed = button.Text;
                        resultButton.PerformClick();
                        //resultValue = resultValue + Double.Parse(resultTB.Text);
                        resultLabel.Text = resultValue + " " + operationPerformed;
                        operrationIsPressed = true;
                    }
                    else
                    {
                        if (resultValue.ToString() == resultTB.Text)
                        {
                            operationPerformed = button.Text;
                            resultValue = Double.Parse(resultTB.Text);
                            resultLabel.Text = resultValue + " " + operationPerformed;
                            operrationIsPressed = true;
                        }
                        else
                        {

                            operationPerformed = button.Text;
                            resultValue = resultValue + Double.Parse(resultTB.Text);
                            resultLabel.Text = resultValue + " " + operationPerformed;
                            operrationIsPressed = true;
                        }
                    }
                }
            }
            
          
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            resultValue = 0;
            resultTB.Text = null;
            resultLabel.Text = "";
        }

        private void resultButton_Click(object sender, EventArgs e)
        {
            switch (operationPerformed)
            {
                case "+":
                {
                    resultTB.Text = (resultValue + double.Parse(resultTB.Text)).ToString();
                    break;
                }
                case "-":
                {
                    if (resultValue == 0)
                    {
                        resultTB.Text = double.Parse(resultTB.Text).ToString();
                    }
                    else
                    {
                        resultTB.Text = (resultValue - double.Parse(resultTB.Text)).ToString();
                    }
                    break;
                }
                case "*":
                {
                    resultTB.Text = (resultValue * double.Parse(resultTB.Text)).ToString();
                    break;
                }
                case "/":
                {
                    resultTB.Text = (resultValue / double.Parse(resultTB.Text)).ToString();
                    break;
                }
                case "^2":
                {
                    resultTB.Text = (resultValue * resultValue).ToString();
                    break;
                }
                case "%":
                {
                    resultTB.Text = (resultValue%double.Parse(resultTB.Text)).ToString();
                    break;
                }
                case "√":
                {
                    resultTB.Text = (Math.Sqrt(resultValue)).ToString();
                    break;
                }
                case "1/":
                {
                    resultTB.Text = (1/resultValue).ToString();
                    break;
                }
                default:
                    break;
            }
            resultValue = double.Parse(resultTB.Text);
            resultLabel.Text = "";
        }

        private void sqrButton_Click(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            operationPerformed = "^2";
            resultValue = Double.Parse(resultTB.Text);
            resultLabel.Text = resultValue + " " + operationPerformed;
            operrationIsPressed = true;

        }

        private void reverseButton_Click(object sender, EventArgs e)
        {
            Button button = (Button) sender;
            operationPerformed = "1/";
            resultValue = Double.Parse(resultTB.Text);
            resultLabel.Text = operationPerformed + resultValue;
            operrationIsPressed = true;
        }

        private void backButton_Click(object sender, EventArgs e)
        {
            resultTB.Text = resultTB.Text.Remove(resultTB.Text.Length - 1);
        }

        private void signButton_Click(object sender, EventArgs e)
        {
            operrationIsPressed = false;
            Button button = (Button)sender;
            
                if (!resultTB.Text.Contains("-"))
                {
                    resultTB.Text = resultTB.Text.Insert(0,"-");
                }
            else
                resultTB.Text = resultTB.Text.Remove(0,1);
        }
    }
}
